function createCard(name, description, pictureUrl, start, end, subtitle) {
  return `
    <div class="card mb-3">
      <img src="${pictureUrl}" class="card-img-top" >
      <div class="card-body shadow p-1 mb-0 rounded">
        <h5 class="card-name">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
        <p class="card-text">${description}</p>
        <footer class="card-footer">${start} - ${end}</footer>
        </div>
    </div>
  `;
}

function errorDetected() {
  return `<div>Error: there seems to be a problem fetching your data </div>`;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);
    if (!response.ok) {
      const html = errorDetected();
      const column = document.querySelector(".row");
      column.innerHTML += html;

      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();
      for (let [index, conference] of data.conferences.entries()) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        let indexString = index.toString();
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const subtitle = details.conference.location.name;
          const start = new Date(details.conference.starts).toLocaleDateString();
          const end = new Date(details.conference.ends).toLocaleDateString();
          const html = createCard(
            name,
            description,
            pictureUrl,
            start,
            end,
            subtitle
          );
          const column = document.querySelector(".cardIndex" + indexString);
          column.innerHTML += html;
          //   const starts = new Date(conference.starts).toLocaleDateString()
          //   const column = document.querySelector(".col");
          //   const container = document.querySelector(".container");
          //   container.innerHTML += column;
        }
      }
    }
  } catch (e) {
    console.log("error", e);
    // Figure out what to do if an error is raised
    const html = errorDetected();
    const column = document.querySelector(".row");
    column.innerHTML += html;
  }
});

// window.addEventListener("DOMContentLoaded", async () => {
//     const url = "http://localhost:8000/api/conferences/";

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         //Figure out what to do when response is bad
//         throw new Error("Response not ok");
//         console.log("An error has occured in the response");
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector(".card-title");
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           console.log(details);

//           const conferenceDetail = details.conference;
//           const detailTag = document.querySelector(".card-text");
//           detailTag.innerHTML = conferenceDetail.description;
//           const imageTag = document.querySelector(".card-img-top");
//           imageTag.src = details.conference.location.picture_url;
//         }
//       }
//     } catch (e) {
//       //figure out what to do if error is raised
//       console.error("error", error);
//       // console.log("Double check your code");
//       // document.getElementById("errorMessage").innerHTML = ex;
//     }
//   });
