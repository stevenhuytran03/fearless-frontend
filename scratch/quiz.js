const uppercaseAll = function (strings) {
    const result = [];

    // your for...of loop here
    for (let string of strings){
        result.push(string.toUpperCase())
    }


    return result;
}
