def median(numbers):
    nums_copy = sorted(numbers)

    if len(nums_copy) % 2 == 0:
        half_len = len(nums_copy) // 2 #?
        num1, num2 = nums_copy[half_len - 1: half_len + 1]
        num1 #?
        return (num1 + num2) // 2
    else:
        middle = len(nums_copy) // 2
        return nums_copy[middle]


a = [1, 2, 3, 4]
print(median(a))


def mode(numbers):
    num_count = {}
    for num in numbers:
        if num not in num_count:
            num_count[num] = 0
        num_count[num] += 1

    max = 0
    for k, v in num_count.items():
        if v > max:
            max = v
            max_number = k
    return max_number






b = [3, 2, 1, 3]
print(mode(b))
