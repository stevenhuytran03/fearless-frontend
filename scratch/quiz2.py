class Person:
    def __init__(self, name):
        self.name = name.upper()

    def __str__(self):
        return self.name


class Employee(Person):
    def __init__(self, employee_id, name):
        super().__init__(name)
        self.employee_id = employee_id

    def __str__(self):
        value = super().__str__()
        return value + " #" + str(self.employee_id)

        # or, using formatted strings (f-strings)
        # return f"{value} #{self.employee_id}"


employee = Employee(6666, "boor") #?
